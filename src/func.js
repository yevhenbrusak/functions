const getSum = (str1, str2) => {
  if(typeof str1 != 'string' || typeof str2 != 'string') return false;
  if(str1.length == 0) str1 = '0';
  if(str2.length == 0) str2 = '0';
  if( !(/^\d+$/.test(str1)) || ! (/^\d+$/.test(str2))) return false;
  const result = Number(str1) + Number(str2);
  return String(result);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countOfPosts = 0;
  let countOfComments = 0;
  listOfPosts.forEach(element => {
      if(element['author'] == authorName) countOfPosts++;
      if(element.hasOwnProperty('comments')){
      element['comments'].forEach(comm =>{
         if(comm.author == authorName) countOfComments++;
      });
  }
  });
  return 'Post:' + countOfPosts + ',comments:' + countOfComments;
};

const tickets=(people)=> {
  let result = 0;
  let answer = 'YES';
  people.forEach(element => {
    if(element == 25) result += element;
    else{
      if(result >= (element-25)){
        result += element;
        result -= (element-25);
      }
      else{
        answer = 'NO';
      }
    }
  });

  return answer;
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
